import java.util.concurrent.Callable;


public class TaskImpl<X, Y> implements Task<X, Y>, Callable<X> {
    private Y value;
    private X input;

    public TaskImpl(X input, Y value) {
        this.value = value;
        this.input = input;
    }

    public X run(Y value) {
        return input;
    }

    public X call() {
        return run(value);
    }
}
