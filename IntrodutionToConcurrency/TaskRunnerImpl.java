import java.lang.InterruptedException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;


public class TaskRunnerImpl implements TaskRunner {
    private ExecutorService pool; 

    public TaskRunnerImpl(int capacity) {
        pool = Executors.newFixedThreadPool(capacity);
    }

    public void shutdown() {
        pool.shutdown();
    }

    public <X, Y> X run(Task<X, Y> task, Y value) {
        try {
            return runCallable((Callable<X>)task);
        } catch (InterruptedException|ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    private <X> X runCallable(Callable<X> task)
            throws InterruptedException, ExecutionException {
        Future<X> result = pool.submit(task);
        return result.get();
    }
}
