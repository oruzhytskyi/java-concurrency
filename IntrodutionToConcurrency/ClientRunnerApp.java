import java.lang.Thread;
import java.lang.InterruptedException;


public class ClientRunnerApp {
    public static void main (String[] args) {
        TaskRunnerImpl taskRunner = new TaskRunnerImpl(2);

        Client client1 = new Client(5, taskRunner);
        Client client2 = new Client(10, taskRunner);

        Thread client1Thread = new Thread(client1);
        Thread client2Thread = new Thread(client2);

        client1Thread.start();
        client2Thread.start();

        try {
            client1Thread.join();
            client2Thread.join();
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }

        taskRunner.shutdown();
    }
}
