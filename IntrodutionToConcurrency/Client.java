import java.lang.Runnable;


public class Client implements Runnable {
    private int taskNumber;
    private TaskRunner taskRunner;

    public Client(int taskNumber, TaskRunner taskRunner) {
        this.taskNumber = taskNumber;
        this.taskRunner = taskRunner;
    }

    public void run() {
//        for (int i = 0; i < taskNumber; i++) {
        int i = 0;
        while (true) {
            int result = taskRunner.run(new TaskImpl<Integer, Integer>(i, i), i);
            i++;
            System.out.println(result);
        }
    }
}
